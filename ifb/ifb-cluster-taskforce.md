template: title

# IFB cluster taskforce

---

template: content

# IFB cluster taskforce

## The team

.pure-table.pure-table-bordered.smaller-font[
| Name | Lab | Localisation |
| --- | --- | --- |
| David Benaben | CBIB | Bordeaux |
| Nicole Charrière | 100% IFB Core | Jouys-en-Josas |
| François Gerbes | 100% IFB Core | Roscoff |
| Jean-Christophe Haessig | IGBMC | Strasbourg |
| Didier Laborie | GenoToul | Toulouse |
| Gildas Le Corguillé | ABiMS | Roscoff |
| Julien Seiler | IGBMC | Strasbourg |
| Guillaume Seith | IGBMC | Strasbourg |
]

---

template: content

# IFB cluster taskforce

## Services
 - [HPC Cluster](https://ifb-elixirfr.gitlab.io/cluster/doc/)
 - [RStudio](https://rstudio.cluster.france-bioinformatique.fr)
 - [usegalaxy.fr](https://usegalaxy.fr/)
 - [Community support](https://community.cluster.france-bioinformatique.fr)

---

template: content

# IFB cluster taskforce

## We Need You!
 - Community support - Expert teams
 - Scientists
 - Bioinformaticians
 - Developers
 - Beta testers

---

template: content

# IFB cluster taskforce - Billing

.callout.callout-warning[Work in progress]

### Why?
 - The IFB founders request 10% of self-sufficient
 - A guarantee of the sustainability of the infrastructure
 - Discipline users to ensure reasonable use of the resources

### How?
Still several scenarios and ideas:
 - A bill per project
 - Different packages/bundles:
   - Freemium (250GB / 10k hours CPU)
   - Small, Medium, Large
   - storage-intensive, cpu_intensive, mix



