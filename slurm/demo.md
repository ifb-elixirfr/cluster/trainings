# Let's try a real life example

We will run alignments with `bowtie2` on 6 Arabidopsis thaliana sequences (single end).

Each sequence is between 300 and 400MB.

Let's see if we can run all 6 alignments in **less than 2 minutes**.

We will use a [Jupyter notebook](files/slurm.ipynb) to illustrate this example.
