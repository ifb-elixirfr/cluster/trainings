template: content

# Job control

## Modify a job
<br/>
.callout.callout-warning[Only if it is pending]
<br/><br/>

`scontrol update jobid=<jobid> partition=<other_partition> account=<other_account> numcpus=<number_of_total_cpus>`<br/>

---

template: content

# Job control

## Prevent a job from starting

<br/>
.callout.callout-warning[Only if it is pending]
<br/><br/>

`scontrol hold <jobid>`

`scontrol release <jobid>`

---

template: content

# Job control

## Dependencies between jobs

Slurm lets you define dependencies between jobs in order to manage task ordering.

`sbatch --dependency=afterok:<other_jobid> <script>`<br/>
*Start this job only after `<other_jobid>` has finished successfully.*

`sbatch --dependency=afternotok:<other_jobid> <script>`<br/>
*Start this job only after `<other_jobid>` has failed.*

`sbatch --dependency=after:<other_jobid> <script>`<br/>
*Start this job only after `<other_jobid>` has started (control starting order)*

`sbatch --dependency=afterany:<other_jobid> <script>`<br/>
*Start this job only after `<other_jobid>` has finished (what ever exit code)*

---

layout: true
name: exercise-dependency

# Job control

## Exercise

Imagine we start the following pipeline:
```
$ sbatch data_download.sh
Submitted batch job 1
$ sbatch --dependency=afterok:1 data_analysis.sh
Submitted batch job 2
```

---

template: exercise-dependency

**Q1: After job `1` has started, what will be the status of job `2` ?**

--

Job `2` will be in `pending` status because it is waiting for job `1` to finish successfully.

--

**Q2: How can you check the status of job `2` ?**

--

```
$ squeue -j 2
JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
    2      fast data_ana  jseiler PD       0:00      1 cpu-node1(Dependency)
```
---

template: exercise-dependency

We realize that we targetted the wrong data in the first job.

**Q3: How can you cancel job `1` ?**

--

```
$ scancel 1
```

--

**Q4: What will be the status of job `2` ?**

Job `2` is still `pending`.

--

```
$ squeue
JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
    2      fast data_ana  jseiler PD       0:00      1 cpu-node1(DependencyNeverSatisfied)
```

---

template: exercise-dependency

We have fixed the `data_download.sh` script.

**Q5: How can we restart the script and update the dependency on job `2` ?**

--

```
$ sbatch --hold data_download.sh
Submitted batch job 3
$ scontrol update jobid=2 dependency=afterok:3
$ scontrol release 3
$ squeue
JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
    2      fast data_ana  jseiler PD       0:00      1 cpu-node1(Dependency)
    3      fast data_dow  jseiler  R       0:33      1 cpu-node1
```
