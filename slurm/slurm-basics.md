template: title

# The SLURM Batch system


---


# SLURM components

.center[![SLURM components](images/slurm-intro.png)]

---

# Basic SLURM concepts

**Account**: A logical group of users

Resource consumption is associated with an account.<br/>
On the IFB cluster an account is created for each project.<br/>
You may have access to multiple accounts (if you have several projects).<br/>
Your first project will be your default account.

--

**Resources**: nodes, CPUs, memory

--

**Partition**: logical group of nodes

They are different partitions on the IFB cluster:
* `fast`: limited to 1 day - 71 nodes available
* `long`: limited to 30 days - 21 nodes available
* `bigmem`: limited to 60 days - 1 node available - On demand
* `training`: limited to 1 day - 5 nodes available - On demand

The default partition is `fast`.

---

# Basic SLURM concepts

## Job

**User's point of view**: a calculation or data analysis

It could be a single program or a full pipeline (a succession of programs).

**Slurm's point of view**: an allocation of resources

--

## Job steps

The processes that actually do the real work

--

## Process/Task

An instance of a computer program executed by one or many threads

--

## Thread

A thread of execution is the smallest sequence of programmed instructions that can be executed

---

# Basic SLURM concepts

.center[![A job](images/a_job.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/processes.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/threads.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/multijobs.drawio.png)]



---
name: srun
layout: true

# Submitting a job... with `srun`

```
$ srun tar -xvzf my_big_data.tar.gz
```

---
template: srun

.center[![SLURM components](images/slurm-srun-1.png)]

---
template: srun

.center[![SLURM components](images/slurm-srun-2.png)]

---
template: srun

.center[![SLURM components](images/slurm-srun-3.png)]

---
template: srun

.center[![SLURM components](images/slurm-srun-4.png)]

---
template: srun

.center[![SLURM components](images/slurm-srun-5.png)]

---
layout: true

---

# `srun` in brief

```bash
srun <command>
```

Outputs comes in your console directly<br>
**The console is blocked** while your job is running

### Default settings :
* 1 CPU core
* 2 GB RAM

### Common parameters :
`--cpus-per-task=` : number of CPU for a single process<br>
`--mem` : memory for the whole job<br>
`--mem-per-cpu` : memory per CPU


---
# Submitting a job... with `sbatch`

Most of the time, you don't want to run a single command and don't want to wait for each command to end to start the next one.
What you want is running a batch script !

A batch script can be any shell script (Bash, R, Python etc.) but most of the time we use **Bash**.

Here is a simple example : `my_script.sh`

```bash
#!/bin/bash
srun tar -xvzf my_data.tar.gz
srun analyse my_data
```

_The shebang is mandatory_

---

layout: true
name: sbatch

# Submitting a job... with `sbatch`

```
$ sbatch my_script.sh
```

---
template: sbatch

.center[![SLURM components](images/slurm-sbatch-1.png)]

---
template: sbatch

.center[![SLURM components](images/slurm-sbatch-2.png)]

---
template: sbatch

.center[![SLURM components](images/slurm-sbatch-3.png)]

---
template: sbatch

.center[![SLURM components](images/slurm-sbatch-4.png)]

---
template: sbatch

.center[![SLURM components](images/slurm-sbatch-5.png)]

---
layout: true

---

# `sbatch` in brief

```bash
sbatch <command>
```

`sbatch` is NOT blocking the console<br>
Outputs comes in files (`slurm-<jobid>.out` and `slurm-<jobid>.err`)<br>
Works ONLY with Shell script

### Default settings :
1 CPU core<br>
2 GB RAM

### Common parameters :
`--cpus-per-task=` : number of CPU for a single process<br>
`--ntasks=` : number of process available for the job<br>
`--mem` : memory for the whole job<br>
`--mem-per-cpu` : memory per CPU

---

# `sbatch` in brief

You can pass `sbatch` parameters in your shell script directly

```bash
#!/bin/bash
#
#SBATCH -p fast                      # partition
#SBATCH --cpus-per-task 10           # number of CPU cores required
#SBATCH --mem 100                    # memory pool for all cores
#SBATCH -t 0-2:00                    # time (D-HH:MM)
#SBATCH -o slurm.%N.%j.out           # STDOUT
#SBATCH -e slurm.%N.%j.err           # STDERR
#SBATCH --mail-type=ALL              # can be BEGIN, END, FAIL or REQUEUE
#SBATCH --mail-user=your@email.com
 
srun bowtie2-build index/chr21.fa index/chr21_hg19
srun bowtie2 --threads $SLURM_CPUS_PER_TASK -x index/chr21_hg19 -U trimmed/chr21_trim.fastq
```

---

# Introduction to SLURM

## Summary

* Get connected to the login node and keep working on it
* For basic command (`cd`, `ls`, `mv`, `mkdir`…), run it directly on the “login node”
* For all the rest, including bioinformatics tools, prepend all command lines by `srun` so that your job will be submitted to the cluster master node and then run on a node of the cluster
* For batch treatment (like pipeline) use `sbatch`
