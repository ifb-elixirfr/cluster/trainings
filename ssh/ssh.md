template: title

# How to access an HPC cluster ?

---

# How to access an HPC cluster?

Only the engineers in charge of maintenance of the cluster are authorized to enter the data center.

**It is thus not possible for users to access these computers with a directly connected keyboard and a local screen**. The connection has to be managed through a **computer network** such as the Internet.

To ensure that resources (nodes, cores, memory) are properly distributed among their users according to their needs, a software called a **Batch system** allows users to **book and access resources**. It is through this software that you can access one or more computers on the HPC cluster.

---

# What do I need to know to use an HPC cluster ?

## Basic Unix

There is no graphical user interface on a HPC cluster.

## How to connect to a remote host through the network?

Your local computer needs to get connected to the cluster!

## How to use a Batch system?

Once connected to the cluster, you need to know how to submit computing jobs.

---

template: content

# What is a shell?

A shell is a **software interface** that allows a user to interact with a computer or server by executing different commands that will return information.

```bash
$ pwd          # print working directory
/shared/home/jseiler
$ ls           # list the files found in the current directory
projets     toto.txt     script.sh
$ cd projets   # change the working directory to the folder named "projets"
$ ls           # list files present in the new working directory
mission-to-mars      rama-II       time-travel
```

The shell allows you to dialog with a local workstation: this assumes that you are physically present in the same room as the machine.

You can also run a shell on a remote computer.

---

# SSH: the remote shell

To interact with a remote computer you need:
* A communication support: computer **network** like Internet
* A communication protocol: **SSH**

***SSH*** (for ***Secure SHell***) is the most commonly used protocol for establishing dialogue and launching commands on a remote machine.

---

# SSH: the remote shell

SSH needs two parameters to run:
* The name or IP of the remote computer
* A user credential (username + password)

Under Linux or Mac, you can run ***SSH*** from the Terminal/Console application:

```bash
$ ssh <username>@<remote-host-name>
```

**Convention:** in the following slides, text between `<` and `>` indicates that the corresponding piece of command should be replaced by a specific content.  

.callout.callout-info[
Under Windows, you can use a Terminal application like ***PuTTY*** or ***MobaXterm***.
]

.callout.callout-warning[
Attention, it is not possible to communicate with **each and every** computer through the SSH protocol. The remote computer **must have** a running SSH service.
]

---

# SSH: the remote shell

## Connect to the IFB HPC cluster

* Remote computer: core.cluster.france-bioinformatique.fr
* User credential: *you received it by email*

Use your Terminal application to connect the cluster:
```
$ ssh <username>@core.cluster.france-bioinformatique.fr
```
*Replace `<username>` with your username.*

You will then be prompted to enter your password (**beware**: at the password prompt, the characters you type are not printed on the screen, for obvious security reasons).

You are now connected to the IFB cluster ***submission node***.

Type `exit` to close the connection.

---

# SSH: the remote shell 2

## Exercise

1. Open an SSH connection to `core.cluster.france-bioinformatique.fr`

2. **Optional:** if this is your first connection, we recommand to change your password with the command `passwd`

3. Print the current working directory.

4. List the files in the working directory.

5. Print the manual of the `df`command;

6. Get the total and the available space (in MB) on the disk where your home directory is located.

---

# SSH: the remote shell

## Copying data between distant computers

SSH allows you to copy data to or from a remote computer with the `scp` command.

General syntax

```bash
scp [source_location] [target_location]
```

* From the local computer to a remote host (Note: *Replace `<username>` with your username.*)


```bash
$ scp /path/to/local/file <username>@<remote-host>:/remote/destination/path
```

* From a remote host to the local computer

```bash
$ scp <username>@<remote-host>:/remote/path /local/path
```

---

# SSH: the remote shell

## Exercise

1. Open an ssh connection to `core.cluster.france-bioinformatique.fr`
2. List the pdf document(s) located in `/shared/projects/du_bii_2019/data/cluster/`
3. Get the full path of the pdf file(s) located there.
4. Copy the PDF document(s) to your local computer

---

# SSH: the remote shell

## Solution to the exercise

1. Connect to `core.cluster.france-bioinformatique.fr`

```bash
local $ ssh seilerj@core.cluster.france-bioinformatique.fr
```
*Replace `seilerj` with your own username (I'm not sharing my password)*

---

# SSH: the remote shell

## Solution to the exercise


1. Connect to `core.cluster.france-bioinformatique.fr`
2. List all files located in `/shared/projects/du_bii_2019/data/cluster/`

```bash
cluster $ ls /shared/projects/du_bii_2019/data/cluster/
```
The result should look like this.
*Do not type this in your terminal, it is not a command ;-)*

```sh
slides.pdf
```

---

# SSH: the remote shell

## Solution to the exercise


1. Connect to `core.cluster.france-bioinformatique.fr`
2. List all files located in `/shared/projects/du_bii_2019/data/cluster/`
3. List only the pdf files in this folder, and get the full path.

```bash
cluster $ ls /shared/projects/du_bii_2019/data/cluster/*.pdf
```

---

# SSH: the remote shell

## Solution to the exercise

1. Open an ssh connection to `core.cluster.france-bioinformatique.fr`
2. List the pdf document(s) located in `/shared/projects/du_bii_2019/data/cluster/`
3. Get the full path of the pdf file(s) located there.
4. Copy the PDF document(s) to your local computer

```bash
local $ scp seilerj@core.cluster.france-bioinformatique.fr:/shared/projects/du_bii_2019/data/cluster/slides.pdf .
```
*Replace `seilerj` with your own username (still not sharing my password)*
